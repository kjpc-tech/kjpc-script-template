# kjpc-script-template

## Basic Python Script Tempalte

---
## KJ PC | [kjpc.tech](https://kjpc.tech/) | [kyle@kjpc.tech](mailto:kyle@kjpc.tech)
---

## Installation (requires [Python3](https://www.python.org/))
1. Install [cookiecutter](https://github.com/cookiecutter/cookiecutter) if not installed
    - `pip3 install cookiecutter`
2. Create project from template
    - `cookiecutter https://gitlab.com/kjpc-tech/kjpc-script-template`
3. Change directories to project root
    - `cd /path/to/new/project/`

### License
[MIT License](LICENSE)
