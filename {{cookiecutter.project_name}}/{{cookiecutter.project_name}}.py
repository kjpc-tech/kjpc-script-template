import argparse
import datetime
import logging


## https://docs.python.org/3/library/logging.html
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(message)s')
file_handler = logging.FileHandler(filename=f"logs/{{cookiecutter.project_name}}{datetime.datetime.now().strftime('%Y-%m-%dT%H:00:00')}.log")
file_handler.setLevel(logging.INFO)
logging.getLogger('').addHandler(file_handler)


## requests-cache is recommended for scripts hitting APIs - https://requests-cache.readthedocs.io/en/stable/
# from requests_cache import CachedSession, NEVER_EXPIRE
# request_session = CachedSession(expire_after=NEVER_EXPIRE, allowable_codes=(200, 404))
# request_session.headers.update({
#     "Authorization": "TODO",
# })


def process(args):
    logging.debug(f'{{cookiecutter.project_name}}: process initiated with {args}')


## https://docs.python.org/3/library/argparse.html
parser = argparse.ArgumentParser(description='{{cookiecutter.project_name}} Script',
                                 epilog='View https://gitlab.com/kjpc-tech/kjpc-script-template for more information.')
parser.add_argument('--process', action='store_true')  # intended to differentiate between a dry run and regular run

if __name__ == "__main__":
    args = parser.parse_args()
    process(args)
    logging.debug('{{cookiecutter.project_name}}: finished')
